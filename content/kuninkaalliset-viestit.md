---
title: "Kuninkaalliset Viestit"
date: 2015-08-15T19:22:12+03:00
draft: false
---

Pari viikkoa sitten löysin [Crypto Puzzle Challenges](http://potatopla.net/crypto/) -sivuston ja pistin sen mieleen jos joskus tulee tylsää ja tässähän sitä taas ollaan.

[Ensimmäisessä tehtävässä](http://potatopla.net/crypto/Challenge_1/) meille annetaan hyvin kryptinen kuva, jossa on jotain merkkijonoja, sekä arvoitus:

    From a royal burgh in West Lothian to the palace on the River Thames
     messages such as this caused a bit of a royal upset.
    /Title

[Thames joki](https://en.wikipedia.org/wiki/River_Thames) on Englannin toiseksi pisin joki joka kulkee Lontoon lävitse useisiin muihin kaupunkeihin, mutta koska arvoituksessa mainitaan palatsi Thamesin varrella on kyseessä luultavimmin Lontoo.

Kun etsin kuninkaallisia "itsehallintokaupunkeja" Länsi Lothiasta, tuli vastaan [Linlithgowia](https://en.wikipedia.org/wiki/Linlithgow) käsittelevä Wikipedia artikkeli, jonka [historia](https://en.wikipedia.org/wiki/Linlithgow#History) otsikon alta löytyi viittaus [Jaakko V](https://en.wikipedia.org/wiki/James_V_of_Scotland) ja [Maria I](https://en.wikipedia.org/wiki/Mary,_Queen_of_Scots). Koska arvoituksessa pyydetään `/Title`, mikä voi ymmärtää myös `/Arvonimenä` lisäsin tehtävän osoitteeseen `/King`, mutta sain vain `404 Not Found` virheen, testasin kuitenkin vielä `/Queen` ja sain seuraavana arvoituksen:

    /Name

Nimeen yritin erilailla kirjoitettuja Mary ja Mary Stuart muotoja, ilman onnea. Kun aloin selaamaan Marian Wikipedia artikkelia osui silmään [kuolema](https://en.wikipedia.org/wiki/Mary,_Queen_of_Scots#Death) kohdan alla "oikeudenkäynti", josta kävi ilmi että, Maria I oli lähettänyt salattuja viestejä, joissa hän yritti saada Elizabeth I salamurhattua. Uudella tiedolla varustettuna lisäsin osoitteen perään `/Elizabeth` ja sain seuraavan arvoituksen:

    /Number

Koska kyseessä oli sekä Maria I että Elizabeth I, kokeilin vielä lisätä osoitteeseen `/1` ja sain seuraavan viestin:

    Congratulations.
    Send [POISTETTU] an email
    with your twitter handle that says you
    are the cheesiest potato.

Challenge 1. oli merkitty *"Aloittelija"* haasteeksi, joten sen ratkaiseminen oli helppoa ja nopeaa, mutta ennen kaikkea se oli hauskaa, ainakin minulle.
